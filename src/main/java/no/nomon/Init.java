package no.nomon;


import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectWriter;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Reading;
import facebook4j.auth.AccessToken;
import no.nomon.helpers.PropertyManager;
import no.nomon.model.AbstractUser;
import no.nomon.model.Role;
import no.nomon.model.Station;
import no.nomon.model.User;
import no.nomon.service.StationService;
import no.nomon.service.UserService;
import no.nomon.service.auth.AuthService;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by aleksandar on 14.11.16..
 */
public class Init {
    public static void init() throws IOException {

        ObjectWriter writer = (new ObjectMapper()).writerWithDefaultPrettyPrinter();

        User admin = new User("Administrator", "admin@nomon.no", "admin", "admin", new HashSet<>(Collections.singletonList(Role.ADMIN)), "admin");
        UserService.INSTANCE.persist(admin);
        System.out.println("Admin user:");
        System.out.println(writer.writeValueAsString(admin));

        AuthService.AuthenticatedUser fbUser = AuthService.INSTANCE.authenticate(new AuthService.FacebookCredentials("EAACX7vfl8ZAMBAIlhX1ziJhbGMZB5za9Mf1cIxZCkQQzYElW6033gY8Jy2Ie6cnn9ZAJRxZAnQhZCeAgbv3XXuqZB47kEZAvCdX3L8cWghvjD8iOOCfYjJ7rx1iJSmscfjZCzfxnLZBvl7ZCb9CZArIxY22S96zaVIZA9XDmXZC2q6NZCYh2qRjqflLImpzfWkZCOyUjRZAtPXapYcldenfdN5vtOp9Ik"));
        System.out.println("Access token for fb user:");
        System.out.println(writer.writeValueAsString(fbUser));

        User ms1 = new User("Moderator School 1", "mod.school1@nomon.no", "ms1", "pass", new HashSet<>(Collections.singletonList(Role.MODERATOR)), "school1");
        User ms2 = new User("Moderator School 2", "mod.school1@nomon.no", "ms2", "pass", new HashSet<>(Collections.singletonList(Role.MODERATOR)), "school2");
        User ms3 = new User("Moderator School 3", "mod.school1@nomon.no", "ms3", "pass", new HashSet<>(Collections.singletonList(Role.MODERATOR)), "school3");
        UserService.INSTANCE.persist(ms1);
        UserService.INSTANCE.persist(ms2);
        UserService.INSTANCE.persist(ms3);
        System.out.println("Moderators for schools:");
        System.out.println(writer.writeValueAsString(ms1));
        System.out.println(writer.writeValueAsString(ms2));
        System.out.println(writer.writeValueAsString(ms3));

        Station station1 = new Station("station 1", "c73a5616-b03a-11e6-80f5-76304dec7eb7");
        StationService.INSTANCE.persist(station1);
        System.out.println("Generating station:");
        System.out.println(writer.writeValueAsString(station1));

    }

    public static void testFacebook() {
        String token = "EAACX7vfl8ZAMBAGsV5PbVrZA7uLKPyDuU1u23DwpZCoP5ziKDFsXKL8aY9H7wPSpWt8bbm2qF2kCw1hXXHwnXauw9SXbrbJ7MSz26BzKSobgeHxgglCZCDLwEKZAs9tXdc2eWemRmSNCNAdiKuTEXkV6X8EwtWsFZByfYZCZCtWf3CHZBhO0O1AJiQPpCYGqZAmZAzIoh3Wi5MU0QZDZD";
        String clientId = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-client-id");
        String clientSecret = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-client-secret");
        String permissions = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-permissions");
        String fields = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-fields");

        Facebook facebook = new FacebookFactory().getInstance();
        facebook.setOAuthAppId(clientId, clientSecret);
        facebook.setOAuthPermissions(permissions);
        facebook.setOAuthAccessToken(new AccessToken(token, null));
        try {
            Reading reading = new Reading();
            reading.fields(fields);
            facebook4j.User me = facebook.getMe(reading);
            System.out.println(me);
        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }
}
