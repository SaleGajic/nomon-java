package no.nomon.dao;

import no.nomon.model.AbstractUser;
import no.nomon.rest.exceptions.NotFound;

import java.util.List;

/**
 * Created by aleksandar on 15.11.16..
 */
public class AbstractUserDAO extends DAO implements DaoInterface<AbstractUser, String> {

    public AbstractUserDAO() {
    }

    public void persist(AbstractUser user) {
        getEntityManager().persist(user);
    }

    public void update(AbstractUser user) {
        getEntityManager().merge(user);
    }

    public AbstractUser findById(String id) throws NotFound {
        AbstractUser user = getEntityManager().find(AbstractUser.class, id);
        if (user == null) throw new NotFound("No record found for id '" + id + "'");
        return user;
    }

    public void delete(AbstractUser user) {
        getEntityManager().remove(user);
    }

    public List<AbstractUser> findAll() {
        return getEntityManager().createQuery("from AbstractUser u", AbstractUser.class).getResultList();
    }

    public int deleteAll() {
        int count = 0;
        for (AbstractUser u : findAll()){
            delete(u);
            count++;
        }
        return count;
    }
}
