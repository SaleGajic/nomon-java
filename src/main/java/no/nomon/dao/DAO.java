package no.nomon.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by aleksandar on 15.11.16..
 */
public abstract class DAO {
    private EntityManager entityManager;
    private EntityTransaction entityTransaction;

    public EntityManager openEntityManager() {
        entityManager = EManager.INSTANCE.getEntityManager();
        return entityManager;
    }

    public EntityManager openEntityManagerWithTransaction() {
        entityManager = EManager.INSTANCE.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        return entityManager;
    }

    public void closeEntityManager(){
        entityManager.close();
    }

    public void closeEntityManagerWithTransaction(){
        entityTransaction.commit();
        entityManager.close();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityTransaction getEntityTransaction() {
        return entityTransaction;
    }

    public void setEntityTransaction(EntityTransaction entityTransaction) {
        this.entityTransaction = entityTransaction;
    }
}
