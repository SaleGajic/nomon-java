package no.nomon.dao;

import no.nomon.rest.exceptions.NotFound;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aleksandar on 15.11.16..
 */
public interface DaoInterface<T, Id extends Serializable> {
    public void persist(T entity);
    public void update(T entity);
    public T findById(Id id) throws NotFound;
    public void delete(T entity);
    public List<T> findAll();
    public int deleteAll();
}