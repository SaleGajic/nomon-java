package no.nomon.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by aleksandar on 14.11.16..
 */
public enum EManager {
    INSTANCE;
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("no.nomon.jpa");

    public EntityManager getEntityManager(){
        return emf.createEntityManager();
    }
}
