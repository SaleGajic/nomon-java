package no.nomon.dao;

import no.nomon.model.FacebookUser;
import no.nomon.rest.exceptions.NotFound;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by aleksandar on 15.11.16..
 */
public class FacebookUserDAO extends DAO implements DaoInterface<FacebookUser, String> {

    public FacebookUserDAO() {
    }

    public void persist(FacebookUser user) {
        getEntityManager().persist(user);
    }

    public void update(FacebookUser user) {
        getEntityManager().merge(user);
    }

    public FacebookUser findById(String id) throws NotFound {
        FacebookUser user = getEntityManager().find(FacebookUser.class, id);
        if (user == null) throw new NotFound("No record found for id '" + id + "'");
        return user;
    }

    public FacebookUser findByFacebookId(String fbid) throws NotFound {
        FacebookUser user = null;
        try {
            user = getEntityManager()
                    .createQuery("SELECT u from FacebookUser u WHERE u.facebookId = :fbid", FacebookUser.class)
                    .setParameter("fbid", fbid).getSingleResult();
        } catch (NoResultException e) {
            throw new NotFound("No record found for fbid '" + fbid + "'");
        }
        return user;
    }

    public void delete(FacebookUser user) {
        getEntityManager().remove(user);
    }

    public List<FacebookUser> findAll() {
        return getEntityManager().createQuery("from FacebookUser u", FacebookUser.class).getResultList();
    }

    public int deleteAll() {
        int count = 0;
        for (FacebookUser u : findAll()){
            delete(u);
            count++;
        }
        return count;
    }
}
