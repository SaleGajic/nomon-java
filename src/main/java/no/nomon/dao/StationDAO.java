package no.nomon.dao;

import no.nomon.model.Station;
import no.nomon.rest.exceptions.NotFound;

import java.util.List;

/**
 * Created by aleksandar on 20.11.16..
 */
public class StationDAO extends DAO implements DaoInterface<Station,String>{

    public void persist(Station entity) {
        getEntityManager().persist(entity);
    }

    public void update(Station entity) {
        getEntityManager().merge(entity);
    }

    public Station findById(String id) throws NotFound {
        Station station = getEntityManager().find(Station.class, id);
        if (station == null) throw new NotFound("No record found for id '" + id + "'");
        return station;
    }

    public void delete(Station entity) {
        getEntityManager().remove(entity);
    }

    public List<Station> findAll() {
        return (List<Station>) getEntityManager().createQuery("from Station s", Station.class).getResultList();
    }

    public int deleteAll() {
        int count = 0;
        for (Station s : findAll()){
            delete(s);
            count++;
        }
        return count;
    }
}
