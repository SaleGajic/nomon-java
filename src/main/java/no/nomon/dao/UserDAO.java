package no.nomon.dao;

import no.nomon.model.User;
import no.nomon.rest.exceptions.NotFound;

import java.util.List;

/**
 * Created by aleksandar on 15.11.16..
 */
public class UserDAO extends DAO implements DaoInterface<User, String> {

    public UserDAO() {
    }

    public void persist(User user) {
        getEntityManager().persist(user);
    }

    public void update(User user) {
        getEntityManager().merge(user);
    }

    public User findById(String id) throws NotFound {
        User user = getEntityManager().find(User.class, id);
        if (user == null) throw new NotFound("No record found for id '" + id + "'");
        return user;
    }

    public User findByUsername(String username) throws NotFound {
        User user = getEntityManager()
                .createQuery("SELECT u from User u WHERE u.username = :username",User.class)
                .setParameter("username",username).getSingleResult();
        if (user == null) throw new NotFound("No record found for username '" + username + "'");
        return user;
    }

    public void delete(User user) {
        getEntityManager().remove(user);
    }

    public List<User> findAll() {
        return getEntityManager().createQuery("from User u", User.class).getResultList();
    }

    public int deleteAll() {
        int count = 0;
        for (User u : findAll()){
            delete(u);
            count++;
        }
        return count;
    }
}
