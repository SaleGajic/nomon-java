package no.nomon.helpers;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by aleksandar on 20.11.16..
 */
public enum PropertyManager {
    INSTANCE;
    private Properties properties = new Properties();

    public Properties getProperties() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        if(properties.isEmpty()){
            try {
                properties.loadFromXML(loader.getResourceAsStream("META-INF/properties.xml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }
}
