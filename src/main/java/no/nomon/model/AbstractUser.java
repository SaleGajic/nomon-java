package no.nomon.model;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aleksandar on 14.11.16..
 */
@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@JsonIgnoreProperties(ignoreUnknown = true)
@GenericGenerator(name = "uuid", strategy = "uuid2")
public class AbstractUser implements Serializable {
    protected String _id;
    protected String name;
    protected String email;
    protected String username;
    protected String password;
    protected Set<String> roles = new HashSet<String>();
    protected String group;

    public AbstractUser() {
    }

    public AbstractUser(String name, String email, String username, String password, Set<String> roles, String group) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.group = group;
    }

    @Id
    @GeneratedValue(generator = "uuid")
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(unique = true, nullable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_roles")
    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public AbstractUser addRole(String role) {
        if (roles == null) {
            roles = new HashSet<String>(Arrays.asList(role));
        } else if (!roles.contains(role)) {
            roles.add(role);
        }
        return this;
    }

    @Column(name = "user_group")
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
