package no.nomon.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Set;

/**
 * Created by aleksandar on 14.11.16..
 */
@Entity
@DiscriminatorValue(value = "facebook")
public class FacebookUser extends AbstractUser {
    private String facebookId;
    private String accessToken;

    public FacebookUser() {
        super();
    }

    public FacebookUser(String name, String email, String username, Set<String> roles, String group, String facebookId, String accessToken) {
        super(name, email, username, null, roles, group);
        this.facebookId = facebookId;
        this.accessToken = accessToken;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
