package no.nomon.model;

/**
 * Created by aleksandar on 18.11.16..
 */
public interface Role {

    String ADMIN = "admin";
    String MODERATOR = "moderator";
    String USER = "user";
    String APP_USER = "app_user";
}