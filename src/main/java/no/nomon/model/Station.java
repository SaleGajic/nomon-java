package no.nomon.model;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by aleksandar on 14.11.16..
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Station implements Serializable {

    private String _id;
    private String key;
    private String name;

    public Station() {
    }

    public Station(String name, String key) {
        this.name = name;
        this.key = key;
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Column(name = "api_key", unique = true)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
