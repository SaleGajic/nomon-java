package no.nomon.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by aleksandar on 14.11.16..
 */
@Entity
@DiscriminatorValue(value = "local")
public class User extends AbstractUser implements Serializable {
    public User() {
    }

    public User(String name, String email, String username, String password, Set<String> roles, String group) {
        super(name, email, username, password, roles, group);
    }
}
