package no.nomon.rest;

import no.nomon.rest.exceptions.Forbidden;
import no.nomon.rest.exceptions.NotFound;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import java.io.Serializable;
import java.util.List;

/**
 * Created by aleksandar on 15.11.16..
 */
public interface RestInterface<T, Id extends Serializable> {
    public List<T> get(@Context ContainerRequestContext crc);
    public T get(Id id, @Context ContainerRequestContext crc) throws NotFound;
    public T create(T entity, @Context ContainerRequestContext crc);
    public T update(Id id, T entity, @Context ContainerRequestContext crc) throws NotFound;
    public T partialUpdate(Id id, T entity, @Context ContainerRequestContext crc) throws NotFound;
    public T remove(Id id, @Context ContainerRequestContext crc) throws NotFound;
}
