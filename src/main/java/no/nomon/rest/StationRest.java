package no.nomon.rest;

import no.nomon.model.Role;
import no.nomon.model.Station;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.StationService;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/stations/")
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
@RolesAllowed({Role.ADMIN})
public class StationRest implements RestInterface<Station, String> {

    @GET
    public List<Station> get(@Context ContainerRequestContext crc) {
        List<Station> stationList = StationService.INSTANCE.findAll();
        return stationList;
    }

    @GET
    @Path("/{id}")
    public Station get(@PathParam("id") String id, @Context ContainerRequestContext crc) throws NotFound {
        return StationService.INSTANCE.findById(id);
    }

    @POST
    public Station create(Station station, @Context ContainerRequestContext crc) {
        StationService.INSTANCE.persist(station);
        return station;
    }

    @PUT
    @Path("/{id}")
    public Station update(@PathParam("id") String id, Station station, @Context ContainerRequestContext crc) throws NotFound {
        StationService.INSTANCE.update(id, station);
        return station;
    }

    @POST
    @Path("/{id}")
    public Station partialUpdate(@PathParam("id") String id, Station station, @Context ContainerRequestContext crc) throws NotFound {
        return StationService.INSTANCE.patch(id, station);
    }


    @DELETE
    @Path("/{id}")
    public Station remove(@PathParam("id") String id, @Context ContainerRequestContext crc) throws NotFound {
        return StationService.INSTANCE.delete(id);
    }
}