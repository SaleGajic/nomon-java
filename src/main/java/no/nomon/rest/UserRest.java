package no.nomon.rest;

import no.nomon.model.AbstractUser;
import no.nomon.model.Role;
import no.nomon.rest.auth.annotations.AllowOwner;
import no.nomon.rest.exceptions.Forbidden;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.AbstractUserService;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Path("/users/")
@PermitAll
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
public class UserRest implements RestInterface<AbstractUser, String> {

    @GET
    @RolesAllowed(value = {Role.ADMIN, Role.MODERATOR})
    public List<AbstractUser> get(@Context ContainerRequestContext crc) {
        List<AbstractUser> userList = AbstractUserService.INSTANCE.findAll();
        return userList;
    }

    @GET
    @Path("/{id}")
    @RolesAllowed(value = {Role.ADMIN})
    @AllowOwner(owner=true, ownerField="_id")
    public AbstractUser get(@PathParam("id") String id, @Context ContainerRequestContext crc) throws NotFound {
        return AbstractUserService.INSTANCE.findById(id);
    }

    @POST
    public AbstractUser create(AbstractUser user, @Context ContainerRequestContext crc) {
        AbstractUser requestingUser = (AbstractUser) crc.getProperty("user");

        // HOTFIX: If email is not defined, take it from username.
        // Username is email address when creating user from app
        if(user.getEmail() == null) user.setEmail(user.getUsername());

        // With unauthenticated request, only only user with APP_USER role and app_group can be created
        // Correct wrong input, and avoid throwing error
        if(requestingUser == null){
            user.setRoles(new HashSet<>(Collections.singletonList(Role.APP_USER)));
            user.setGroup(Role.APP_USER);
        }
        else{
            // Let admin do everything, but for others sanitize input
            if(!requestingUser.getRoles().contains(Role.ADMIN)){
                // Moderator can only create user of the same group, and with USER role
                if (requestingUser.getRoles().contains(Role.MODERATOR)){
                    user.setRoles(new HashSet<>(Collections.singletonList(Role.USER)));
                    user.addRole(Role.USER);
                }
                // If authenticated, but not admin nor moderator, set APP_USER
                else{
                    user.setRoles(new HashSet<>(Collections.singletonList(Role.APP_USER)));
                    user.setGroup(Role.APP_USER);
                }
            }
        }
        AbstractUserService.INSTANCE.persist(user);
        return user;
    }

    @PUT
    @Path("/{id}")
    @RolesAllowed(value = {Role.ADMIN, Role.MODERATOR})
    @AllowOwner(owner=true, ownerField="_id")
    public AbstractUser update(@PathParam("id") String id, AbstractUser user, @Context ContainerRequestContext crc) throws NotFound {
        AbstractUserService.INSTANCE.update(id, user);
        return user;
    }

    @POST
    @Path("/{id}")
    @RolesAllowed(value = {Role.ADMIN, Role.MODERATOR})
    @AllowOwner(owner=true, ownerField="_id")
    public AbstractUser partialUpdate(@PathParam("id") String id, AbstractUser user, @Context ContainerRequestContext crc) throws NotFound {
        return AbstractUserService.INSTANCE.patch(id, user);
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed(value = {Role.ADMIN, Role.MODERATOR})
    @AllowOwner(owner=true, ownerField="_id")
    public AbstractUser remove(@PathParam("id") String id, @Context ContainerRequestContext crc) throws NotFound {
        return AbstractUserService.INSTANCE.delete(id);
    }
}