package no.nomon.rest.auth;

import no.nomon.model.User;
import no.nomon.rest.exceptions.NotAuthenticated;
import no.nomon.service.auth.AuthService;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 20.11.16..
 */
@Provider
@Produces({MediaType.APPLICATION_JSON})
@Path("/auth/local")
@PermitAll
public class AuthLocal {

    @POST
    public AuthService.AuthenticatedUser<User> authenticate(AuthService.LocalCredentials credentials) throws NotAuthenticated {
        return AuthService.INSTANCE.authenticate(credentials);
    }
}
