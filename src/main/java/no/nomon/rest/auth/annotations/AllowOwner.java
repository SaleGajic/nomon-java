package no.nomon.rest.auth.annotations;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by aleksandar on 21.11.16..
 */
@Retention(RUNTIME)
public @interface AllowOwner {
    boolean owner();

    String ownerField();
}
