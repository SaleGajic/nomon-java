package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class BadRequest extends NomonException {
    public BadRequest() {
        super("Request is in wrong format", 400, "BadRequest", "bad-request");
    }

    public BadRequest(String message) {
        super(message, 400, "BadRequest", "bad-request");
    }
}
