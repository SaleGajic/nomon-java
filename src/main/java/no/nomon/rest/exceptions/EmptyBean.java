package no.nomon.rest.exceptions;


import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by aleksandar on 19.11.16..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmptyBean implements Serializable {
    private static final long serialVersionUID = 1L;
}
