package no.nomon.rest.exceptions;


import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.beanutils.BeanUtils;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by aleksandar on 19.11.16..
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorMessage implements Serializable {
    private int code;
    private String name;
    private String message;
    private String className;
    private EmptyBean data = new EmptyBean();
    private EmptyBean errors = new EmptyBean();

    public ErrorMessage(NomonException e) {
        try {
            BeanUtils.copyProperties(this, e);
        } catch (IllegalAccessException | InvocationTargetException e1) {
            e1.printStackTrace();
        }
    }

    public ErrorMessage() {
    }

    public ErrorMessage(String message, int code, String name, String className) {
        this.message = message;
        this.className = className;
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public EmptyBean getData() {
        return data;
    }

    public void setData(EmptyBean data) {
        this.data = data;
    }

    public EmptyBean getErrors() {
        return errors;
    }

    public void setErrors(EmptyBean errors) {
        this.errors = errors;
    }
}
