package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class Forbidden extends NomonException {
    public Forbidden() {
        super("You do not have valid permissions to access this.", 400, "Forbidden", "forbidden");
    }

    public Forbidden(String message) {
        super(message, 400, "Forbidden", "forbidden");
    }
}
