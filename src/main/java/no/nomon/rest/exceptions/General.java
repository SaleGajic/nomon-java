package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class General extends NomonException {
    public General() {
        super("General error", 500, "General", "general-error");
    }

    public General(String message) {
        super(message, 500, "General", "general-error");
    }
}
