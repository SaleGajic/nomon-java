package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class MethodNotAllowed extends NomonException {
    public MethodNotAllowed() {
        super("Method you have used is not allowed", 405, "MethodNotAllowed", "method-not-allowed");
    }

    public MethodNotAllowed(String message) {
        super(message, 405, "MethodNotAllowed", "method-not-allowed");
    }
}
