package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by aleksandar on 18.11.16..
 */
@XmlRootElement
@Provider
public class NomonException extends RuntimeException {
    private int code;
    private String name;
    private String message;
    private String className;
    private EmptyBean data = new EmptyBean();
    private EmptyBean errors = new EmptyBean();

    public NomonException(String message, int code, String name, String className) {
        super(message);
        this.code = code;
        this.name = name;
        this.message = message;
        this.className = className;
    }

    public NomonException() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public EmptyBean getData() {
        return data;
    }

    public void setData(EmptyBean data) {
        this.data = data;
    }

    public EmptyBean getErrors() {
        return errors;
    }

    public void setErrors(EmptyBean errors) {
        this.errors = errors;
    }
}
