package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class NotAuthenticated extends NomonException {
    public NotAuthenticated() {
        super("Not authenticated", 401, "NotAuthenticated", "not-authenticated");
    }

    public NotAuthenticated(String message) {
        super(message, 401, "NotAuthenticated", "not-authenticated");
    }
}
