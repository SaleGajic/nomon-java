package no.nomon.rest.exceptions;

import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class NotFound extends NomonException {
    public NotFound() {
        super("Resource not found", 404, "NotFound", "not-found");
    }

    public NotFound(String message) {
        super(message, 404, "NotFound", "not-found");
    }
}
