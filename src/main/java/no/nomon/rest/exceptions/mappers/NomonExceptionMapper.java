package no.nomon.rest.exceptions.mappers;

import no.nomon.rest.exceptions.ErrorMessage;
import no.nomon.rest.exceptions.NomonException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 18.11.16..
 */
@Provider
public class NomonExceptionMapper implements ExceptionMapper<NomonException> {
    public Response toResponse(NomonException e) {
        return Response.status(e.getCode())
                .entity(new ErrorMessage(e))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
