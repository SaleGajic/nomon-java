package no.nomon.rest.exceptions.mappers;

import no.nomon.rest.exceptions.ErrorMessage;

import javax.persistence.RollbackException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.sql.SQLException;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class RollbackExceptionMapper implements ExceptionMapper<RollbackException> {
    public Response toResponse(RollbackException e) {
        SQLException sqlException;
        Throwable t = e;
        while (!(t instanceof SQLException)){
            t = t.getCause();
        }
        sqlException = (SQLException) t;
        return Response.status(500)
                .entity(new ErrorMessage(sqlException.getMessage(), 500, "GeneralServerError", "general-server-error"))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
