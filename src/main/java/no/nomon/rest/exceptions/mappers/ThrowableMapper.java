package no.nomon.rest.exceptions.mappers;

import no.nomon.rest.exceptions.ErrorMessage;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by aleksandar on 19.11.16..
 */
@Provider
public class ThrowableMapper implements ExceptionMapper<Throwable> {
    public Response toResponse(Throwable throwable) {
        throwable.printStackTrace();
        return Response.status(500)
                .entity(new ErrorMessage(throwable.getMessage(), 500, "GeneralServerError", "general-server-error"))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
