package no.nomon.rest.filters;


import no.nomon.model.AbstractUser;
import no.nomon.rest.auth.annotations.AllowOwner;
import no.nomon.rest.exceptions.Forbidden;
import no.nomon.rest.exceptions.NotAuthenticated;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.rest.exceptions.mappers.NomonExceptionMapper;
import no.nomon.service.DbServiceInterface;
import no.nomon.service.ServiceFactory;
import no.nomon.service.auth.AuthService;
import org.apache.commons.beanutils.BeanUtils;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aleksandar on 20.11.16..
 */
@Provider
public class AuthFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Context
    private UriInfo uriInfo;

    private NomonExceptionMapper nomonExceptionMapper = new NomonExceptionMapper();

    private Method method;
    private Class<?> clazz;
    private AbstractUser user;

    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        method = resourceInfo.getResourceMethod();
        clazz = resourceInfo.getResourceClass();
        String token = containerRequestContext.getHeaderString("authorization");
        try {
            user = AuthService.INSTANCE.getUserFromToken(token);
            containerRequestContext.setProperty("user", user);
        } catch (NotAuthenticated notAuthenticated) {
            if (token != null && !token.equalsIgnoreCase("null") && !token.isEmpty() || !isPublic()) {
                containerRequestContext.abortWith(nomonExceptionMapper.toResponse(notAuthenticated));
                return;
            }
        }
        // If method is not public, then check if user has sufficient rights to access this method
        if (!isPublic()) {
            if (!isUserAllowed()) {
                containerRequestContext.abortWith(nomonExceptionMapper.toResponse(new Forbidden()));
            }
        }
    }

    private boolean isUserAllowed() {
        Set<String> classRolesGranted = clazz.isAnnotationPresent(RolesAllowed.class) ?
                new HashSet<String>(Arrays.asList(clazz.getAnnotation(RolesAllowed.class).value())) : new HashSet<String>();
        Set<String> methodRolesGranted = method.isAnnotationPresent(RolesAllowed.class) ?
                new HashSet<String>(Arrays.asList(method.getAnnotation(RolesAllowed.class).value())) : new HashSet<String>();
        for (String role : user.getRoles()) {
            if (classRolesGranted.contains(role) || methodRolesGranted.contains(role)) {
                return true;
            }
        }
        return isOwnerllowed();
    }

    private boolean isOwnerllowed() {
        return method.isAnnotationPresent(AllowOwner.class) &&
                method.getAnnotation(AllowOwner.class).owner() && isOwner();
    }

    private boolean isOwner() {
        String ownerField = method.getAnnotation(AllowOwner.class).ownerField();
        DbServiceInterface service = ServiceFactory.INSTANCE.getService(method.getReturnType());
        switch (method.getName()) {
            case "get":
                String id = uriInfo.getPathParameters().get("id").get(0);
                try {
                    return BeanUtils.getProperty(service.findById(id), ownerField).equals(user.get_id());
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (NotFound notFound) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }

    private boolean isPublic() {
        return method.isAnnotationPresent(PermitAll.class) ||
                clazz.isAnnotationPresent(PermitAll.class) &&
                        !method.isAnnotationPresent(DenyAll.class) &&
                        !method.isAnnotationPresent(RolesAllowed.class);
    }
}
