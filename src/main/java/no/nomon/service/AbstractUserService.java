package no.nomon.service;

import no.nomon.dao.AbstractUserDAO;
import no.nomon.model.AbstractUser;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.helpers.SkipNullBeanUtilsBean;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by aleksandar on 14.11.16..
 */
public enum AbstractUserService implements DbServiceInterface<AbstractUser, String> {
    INSTANCE;
    private AbstractUserDAO userDAO = new AbstractUserDAO();

    public AbstractUser persist(AbstractUser user) {
        userDAO.openEntityManagerWithTransaction();
        userDAO.persist(user);
        userDAO.closeEntityManagerWithTransaction();
        return user;
    }

    public AbstractUser update(String id, AbstractUser user) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        AbstractUser userToUpdate = userDAO.findById(id);
        try {
            BeanUtils.copyProperties(userToUpdate, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDAO.closeEntityManagerWithTransaction();
        return userToUpdate;
    }

    public AbstractUser patch(String id, AbstractUser user) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        AbstractUser userToUpdate = userDAO.findById(id);
        BeanUtilsBean skipNullBean = new SkipNullBeanUtilsBean();
        try {
            skipNullBean.copyProperties(userToUpdate, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDAO.closeEntityManagerWithTransaction();
        return userToUpdate;
    }

    public AbstractUser findById(String id) throws NotFound {
        userDAO.openEntityManager();
        AbstractUser user = userDAO.findById(id);
        userDAO.closeEntityManager();
        return user;
    }


    public AbstractUser delete(String id) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        AbstractUser user = userDAO.findById(id);
        userDAO.delete(user);
        userDAO.closeEntityManagerWithTransaction();
        return user;
    }

    public List<AbstractUser> findAll() {
        userDAO.openEntityManager();
        List<AbstractUser> users = userDAO.findAll();
        userDAO.closeEntityManager();
        return users;
    }

    public int deleteAll() {
        userDAO.openEntityManagerWithTransaction();
        int count = userDAO.deleteAll();
        userDAO.closeEntityManagerWithTransaction();
        return count;
    }
}
