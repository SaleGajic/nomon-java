package no.nomon.service;

import no.nomon.rest.exceptions.NotFound;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aleksandar on 14.11.16..
 */
public interface DbServiceInterface<T, Id extends Serializable> {
    public T persist(T entity);
    public T update(String id, T entity) throws NotFound;
    public T patch(String id, T user) throws NotFound;
    public T findById(Id id) throws NotFound;
    public T delete(Id id) throws NotFound;
    public List<T> findAll();
    public int deleteAll();
}
