package no.nomon.service;

import no.nomon.dao.FacebookUserDAO;
import no.nomon.model.FacebookUser;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.helpers.SkipNullBeanUtilsBean;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by aleksandar on 14.11.16..
 */
public enum FacebookUserService implements DbServiceInterface<FacebookUser, String> {
    INSTANCE;
    private FacebookUserDAO userDAO = new FacebookUserDAO();

    public FacebookUser persist(FacebookUser user) {
        userDAO.openEntityManagerWithTransaction();
        userDAO.persist(user);
        userDAO.closeEntityManagerWithTransaction();
        return user;
    }

    public FacebookUser update(String id, FacebookUser user) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        FacebookUser userToUpdate = userDAO.findById(id);
        try {
            BeanUtils.copyProperties(userToUpdate, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDAO.closeEntityManagerWithTransaction();
        return userToUpdate;
    }

    public FacebookUser patch(String id, FacebookUser user) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        FacebookUser userToUpdate = userDAO.findById(id);
        BeanUtilsBean skipNullBean = new SkipNullBeanUtilsBean();
        try {
            skipNullBean.copyProperties(userToUpdate, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDAO.closeEntityManagerWithTransaction();
        return userToUpdate;
    }

    public FacebookUser findById(String id) throws NotFound {
        userDAO.openEntityManager();
        FacebookUser user = userDAO.findById(id);
        userDAO.closeEntityManager();
        return user;
    }

    public FacebookUser findByFacebookId(String fbid) throws NotFound {
        userDAO.openEntityManager();
        FacebookUser user = userDAO.findByFacebookId(fbid);
        userDAO.closeEntityManager();
        return user;
    }

    public FacebookUser delete(String id) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        FacebookUser user = userDAO.findById(id);
        userDAO.delete(user);
        userDAO.closeEntityManagerWithTransaction();
        return user;
    }

    public List<FacebookUser> findAll() {
        userDAO.openEntityManager();
        List<FacebookUser> users = userDAO.findAll();
        userDAO.closeEntityManager();
        return users;
    }

    public int deleteAll() {
        userDAO.openEntityManagerWithTransaction();
        int count = userDAO.deleteAll();
        userDAO.closeEntityManagerWithTransaction();
        return count;
    }
}
