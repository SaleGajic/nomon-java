package no.nomon.service;

import no.nomon.model.AbstractUser;
import no.nomon.model.FacebookUser;
import no.nomon.model.Station;
import no.nomon.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aleksandar on 21.11.16..
 */
public enum ServiceFactory {
    INSTANCE;
    Map<Class, Enum> serviceMap = new HashMap<Class, Enum>();

    ServiceFactory() {
        serviceMap.put(AbstractUser.class, AbstractUserService.INSTANCE);
        serviceMap.put(User.class, UserService.INSTANCE);
        serviceMap.put(FacebookUser.class, FacebookUserService.INSTANCE);
        serviceMap.put(Station.class, StationService.INSTANCE);
    }

    public DbServiceInterface getService(Class clazz) {
        DbServiceInterface service = (DbServiceInterface) serviceMap.get(clazz);
        if (service == null) {
            throw new RuntimeException("ServiceFactory can't find service for " + clazz);
        }
        return service;
    }
}
