package no.nomon.service;

import no.nomon.dao.StationDAO;
import no.nomon.model.Station;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.helpers.SkipNullBeanUtilsBean;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by aleksandar on 20.11.16..
 */
public enum StationService implements DbServiceInterface<Station, String> {
    INSTANCE;
    private StationDAO stationDAO = new StationDAO();

    public Station persist(Station station) {
        stationDAO.openEntityManagerWithTransaction();
        stationDAO.persist(station);
        stationDAO.closeEntityManagerWithTransaction();
        return station;
    }

    public Station update(String id, Station station) throws NotFound {
        stationDAO.openEntityManagerWithTransaction();
        Station stationToUpdate = stationDAO.findById(id);
        try {
            BeanUtils.copyProperties(stationToUpdate, station);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        stationDAO.closeEntityManagerWithTransaction();
        return stationToUpdate;
    }

    public Station patch(String id, Station station) throws NotFound {
        stationDAO.openEntityManagerWithTransaction();
        Station stationToUpdate = stationDAO.findById(id);
        BeanUtilsBean skipNullBean = new SkipNullBeanUtilsBean();
        try {
            skipNullBean.copyProperties(stationToUpdate, station);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        stationDAO.closeEntityManagerWithTransaction();
        return stationToUpdate;
    }

    public Station findById(String id) throws NotFound {
        stationDAO.openEntityManager();
        Station station = stationDAO.findById(id);
        stationDAO.closeEntityManager();
        return station;
    }

    public Station delete(String id) throws NotFound {
        stationDAO.openEntityManagerWithTransaction();
        Station station = stationDAO.findById(id);
        stationDAO.delete(station);
        stationDAO.closeEntityManagerWithTransaction();
        return station;
    }

    public List<Station> findAll() {
        stationDAO.openEntityManager();
        List<Station> stations = stationDAO.findAll();
        stationDAO.closeEntityManager();
        return stations;
    }

    public int deleteAll() {
        stationDAO.openEntityManagerWithTransaction();
        int count = stationDAO.deleteAll();
        stationDAO.closeEntityManagerWithTransaction();
        return count;
    }
}
