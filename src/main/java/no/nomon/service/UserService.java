package no.nomon.service;

import no.nomon.dao.UserDAO;
import no.nomon.model.User;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.helpers.SkipNullBeanUtilsBean;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by aleksandar on 14.11.16..
 */
public enum UserService implements DbServiceInterface<User, String> {
    INSTANCE;
    private UserDAO userDAO = new UserDAO();

    public User persist(User user) {
        userDAO.openEntityManagerWithTransaction();
        userDAO.persist(user);
        userDAO.closeEntityManagerWithTransaction();
        return user;
    }

    public User update(String id, User user) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        User userToUpdate = userDAO.findById(id);
        try {
            BeanUtils.copyProperties(userToUpdate, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDAO.closeEntityManagerWithTransaction();
        return userToUpdate;
    }

    public User patch(String id, User user) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        User userToUpdate = userDAO.findById(id);
        BeanUtilsBean skipNullBean = new SkipNullBeanUtilsBean();
        try {
            skipNullBean.copyProperties(userToUpdate, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDAO.closeEntityManagerWithTransaction();
        return userToUpdate;
    }

    public User findById(String id) throws NotFound {
        userDAO.openEntityManager();
        User user = userDAO.findById(id);
        userDAO.closeEntityManager();
        return user;
    }

    public User findByUsername(String username) throws NotFound {
        userDAO.openEntityManager();
        User user = userDAO.findByUsername(username);
        userDAO.closeEntityManager();
        return user;
    }

    public User delete(String id) throws NotFound {
        userDAO.openEntityManagerWithTransaction();
        User user = userDAO.findById(id);
        userDAO.delete(user);
        userDAO.closeEntityManagerWithTransaction();
        return user;
    }

    public List<User> findAll() {
        userDAO.openEntityManager();
        List<User> users = userDAO.findAll();
        userDAO.closeEntityManager();
        return users;
    }

    public int deleteAll() {
        userDAO.openEntityManagerWithTransaction();
        int count = userDAO.deleteAll();
        userDAO.closeEntityManagerWithTransaction();
        return count;
    }
}
