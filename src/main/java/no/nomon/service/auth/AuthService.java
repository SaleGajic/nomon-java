package no.nomon.service.auth;

import no.nomon.dao.EManager;
import no.nomon.model.AbstractUser;
import no.nomon.model.FacebookUser;
import no.nomon.model.User;
import no.nomon.rest.exceptions.NotAuthenticated;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.UserService;

import javax.persistence.NoResultException;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by aleksandar on 20.11.16..
 */
public enum AuthService {
    INSTANCE;

    @XmlRootElement
    public static class LocalCredentials {
        private String username;
        private String password;

        public LocalCredentials() {
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public static class FacebookCredentials {
        private String access_token;

        public FacebookCredentials() {
        }

        public FacebookCredentials(String access_token) {
            this.access_token = access_token;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }
    }

    @XmlRootElement
    public static class AuthenticatedUser<T> {
        private String token;
        private T data;

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public AuthenticatedUser(String token, T data) {

            this.data = data;
            this.token = token;
        }

        public AuthenticatedUser() {

        }
    }

    public AuthenticatedUser<User> authenticate(LocalCredentials credentials) throws NotAuthenticated {
        User user = null;
        try {
            user = UserService.INSTANCE.findByUsername(credentials.getUsername());
            if (!user.getPassword().equals(credentials.getPassword())) {
                throw new NotAuthenticated("Invalid credentials");
            }
        } catch (NoResultException | NotFound e) {
            throw new NotAuthenticated("Invalid credentials");
        }
        return new AuthenticatedUser<>(NomonJWT.INSTANCE.issue(user.get_id()), user);
    }

    public AuthenticatedUser<FacebookUser> authenticate(FacebookCredentials credentials) throws NotAuthenticated {
        FacebookUser user = null;
        user = FacebookAuthentication.INSTANCE.authenticate(credentials.getAccess_token());
        return new AuthenticatedUser<>(NomonJWT.INSTANCE.issue(user.get_id()), user);
    }

    public AbstractUser getUserFromToken(String token) throws NotAuthenticated {
        String userId = NomonJWT.INSTANCE.verify(token);
        AbstractUser user = null;
        if (userId == null) {
            throw new NotAuthenticated("Wrong JWT token");
        }
        user = EManager.INSTANCE.getEntityManager().find(AbstractUser.class, userId);

        if (user == null) {
            throw new NotAuthenticated("Wrong JWT token");
        }
        return user;
    }
}
