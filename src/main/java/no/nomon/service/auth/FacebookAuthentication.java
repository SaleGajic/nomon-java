package no.nomon.service.auth;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Reading;
import facebook4j.auth.AccessToken;
import no.nomon.helpers.PropertyManager;
import no.nomon.model.FacebookUser;
import no.nomon.model.Role;
import no.nomon.rest.exceptions.NotAuthenticated;
import no.nomon.rest.exceptions.NotFound;
import no.nomon.service.FacebookUserService;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by aleksandar on 21.11.16..
 */
public enum FacebookAuthentication {
    INSTANCE;
    private String clientId = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-client-id");
    private String clientSecret = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-client-secret");
    private String permissions = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-permissions");
    private String fields = PropertyManager.INSTANCE.getProperties().getProperty("Facebook-fields");
    private Facebook facebook = new FacebookFactory().getInstance();
    private Reading reading = new Reading();

    FacebookAuthentication() {
        facebook.setOAuthAppId(clientId, clientSecret);
        facebook.setOAuthPermissions(permissions);
        reading.fields(fields);
    }

    public FacebookUser authenticate(String accessToken) throws NotAuthenticated {
        facebook.setOAuthAccessToken(new AccessToken(accessToken, null));
        FacebookUser user = null;
        try {
            facebook4j.User fbApiUser = facebook.getMe(reading);
            user = new FacebookUser(fbApiUser.getName(), fbApiUser.getEmail(), fbApiUser.getEmail(), new HashSet(Collections.singletonList(Role.APP_USER)), Role.APP_USER, fbApiUser.getId(), accessToken);
            String user_id = FacebookUserService.INSTANCE.findByFacebookId(fbApiUser.getId()).get_id();
            user.set_id(user_id);
            FacebookUserService.INSTANCE.patch(user_id, user);
        } catch (FacebookException e) {
            throw new NotAuthenticated("Invalid facebook access token");
        } catch (NotFound notFound) {
            FacebookUserService.INSTANCE.persist(user);
        }
        return user;
    }

}
