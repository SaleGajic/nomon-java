package no.nomon.service.auth;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import no.nomon.helpers.PropertyManager;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aleksandar on 20.11.16..
 */
public enum NomonJWT {
    INSTANCE;

    private static final String issuer = PropertyManager.INSTANCE.getProperties().getProperty("JWT-issuer");
    private static final String secret = PropertyManager.INSTANCE.getProperties().getProperty("JWT-secret");

    public String issue(String id) {
        final long iat = System.currentTimeMillis() / 1000L; // issued at claim
        final long exp = iat + 24L * 3600L; // expires claim. In this case the token expires in 60 seconds


        final JWTSigner signer = new JWTSigner(secret);
        final HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("iss", issuer);
        claims.put("exp", exp);
        claims.put("iat", iat);
        claims.put("_id", id);

        return signer.sign(claims);
    }

    public String verify(String token) {
        String id = null;
        try {
            final JWTVerifier verifier = new JWTVerifier(secret);
            final Map<String, Object> claims = verifier.verify(token);
            final long now = System.currentTimeMillis() / 1000L;
            final long exp = (Integer) claims.get("exp");
            if (exp < now) {
                return null;
            }
            id = (String) claims.get("_id");
        } catch (JWTVerifyException e) {
            // Invalid Token
        } catch (NoSuchAlgorithmException e) {
        } catch (IOException e) {
        } catch (SignatureException e) {
        } catch (InvalidKeyException e) {
        } catch (IllegalStateException e){
        }
        return id;
    }
}
