package no.nomon.test.model;

import junit.framework.TestCase;
import no.nomon.helpers.PropertyManager;

import java.util.Properties;

/**
 * Created by aleksandar on 14.11.16..
 */
public class PropertiesTest extends TestCase {

    public void test() {
        Properties p = PropertyManager.INSTANCE.getProperties();
        assertEquals("world", p.getProperty("hello"));
        assertEquals("planet", p.getProperty("java"));
    }
}
