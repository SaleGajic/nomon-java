package no.nomon.test.model;

import junit.framework.TestCase;
import no.nomon.model.Station;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by aleksandar on 14.11.16..
 */
public class StationTest extends TestCase {
    private EntityManagerFactory entityManagerFactory;

    @Override
    protected void setUp() throws Exception {
        entityManagerFactory = Persistence.createEntityManagerFactory("no.nomon.jpa");
    }

    @Override
    protected void tearDown() throws Exception {
        entityManagerFactory.close();
    }


    public void testCreateAndDelete() {
        Station testStation = new Station("Init station 1", "12345-1234-12334-12345");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // Init creating station

        entityManager.getTransaction().begin();
        entityManager.persist(testStation);
        entityManager.getTransaction().commit();

        Station station = entityManager.createQuery("SELECT s from Station s where s.key = :key", Station.class).setParameter("key", testStation.getKey()).getSingleResult();
        System.out.println("Station (" + station.getName() + ") : " + station.getKey() + " - " + station.get_id());
        assertEquals(testStation.getName(), station.getName());

        // Init deleting station

        entityManager.getTransaction().begin();
        entityManager.remove(station);
        entityManager.getTransaction().commit();

        List<Station> stations = entityManager.createQuery("SELECT s from Station s where s.key = :key", Station.class).setParameter("key", testStation.getKey()).getResultList();
        assertEquals(0, stations.size());

        entityManager.close();

    }
}
