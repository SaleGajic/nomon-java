package no.nomon.test.model;

import junit.framework.TestCase;
import no.nomon.model.FacebookUser;
import no.nomon.model.Role;
import no.nomon.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by aleksandar on 14.11.16..
 */
public class UserTest extends TestCase {
    private EntityManagerFactory entityManagerFactory;

    @Override
    protected void setUp() throws Exception {
        entityManagerFactory = Persistence.createEntityManagerFactory("no.nomon.jpa");
    }

    @Override
    protected void tearDown() throws Exception {
        entityManagerFactory.close();
    }


    public void testCreateAndDelete() {
        User testUser = new User("Aleksandar Gajic", "sasa.gajic91@gmail.com", "sasa.gajic91@gmail.com", "password123", new HashSet(Collections.singletonList(Role.ADMIN)), "admin");
        FacebookUser facebooTestkUser = new FacebookUser("Aleksandar Gajic", "sasa.gajic@ikomline.net", "sasa.gajic@ikomline.net", null, null, "10210767648084216", "EAACX7vfl8ZAMBAIlhX1ziJhbGMZB5za9Mf1cIxZCkQQzYElW6033gY8Jy2Ie6cnn9ZAJRxZAnQhZCeAgbv3XXuqZB47kEZAvCdX3L8cWghvjD8iOOCfYjJ7rx1iJSmscfjZCzfxnLZBvl7ZCb9CZArIxY22S96zaVIZA9XDmXZC2q6NZCYh2qRjqflLImpzfWkZCOyUjRZAtPXapYcldenfdN5vtOp9Ik");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // Init creating user

        entityManager.getTransaction().begin();
        entityManager.persist(testUser);
        entityManager.persist(facebooTestkUser);
        entityManager.getTransaction().commit();

        User user = entityManager.createQuery("SELECT u from User u where u.username = :username", User.class)
                .setParameter("username", testUser.getUsername()).getSingleResult();
        System.out.println("User (" + user.getUsername() + ") : " + user.getName() + " - " + user.get_id());
        assertEquals(testUser.getName(), user.getName());

        FacebookUser facebookUser = entityManager.createQuery("SELECT u from FacebookUser u where u.username = :username", FacebookUser.class)
                .setParameter("username", facebooTestkUser.getUsername()).getSingleResult();
        System.out.println("FacebookUser (" + facebookUser.getUsername() + ") : " + facebookUser.getName() + " - " + facebookUser.get_id() + " - " + facebookUser.getFacebookId());
        assertEquals(facebooTestkUser.getName(), facebookUser.getName());

        // Init deleting users

        entityManager.getTransaction().begin();
        entityManager.remove(user);
        entityManager.remove(facebookUser);
        entityManager.getTransaction().commit();

        List<User> users = entityManager.createQuery("SELECT u from User u where u.username = :username or u.username = :fbUsername", User.class)
                .setParameter("username", testUser.getUsername())
                .setParameter("fbUsername", facebooTestkUser.getUsername())
                .getResultList();
        assertEquals(0, users.size());

        entityManager.close();

    }
}
